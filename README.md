### Start the project

- use `npm i` to install all dependencies.
- use `npm start` to start the project in development mode.
- use `npm build` to build the project for production.

### Env

Copy `.env.sample` as `.env` and don't forget to fill variables, especially `REACT_APP_CLUES_API_URL`.
You can specify the port where app starts by setting `PORT` env variable.
Please learn more `.env.sample`-file to see all variables that you can change.
