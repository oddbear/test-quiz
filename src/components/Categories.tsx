import React, { useCallback, useEffect } from 'react';
import useLocalStorage from 'use-local-storage';
import { requestToApi } from '../utils/request';
import { ClueCategory } from '../types/Clue.type';
import styled from 'styled-components';
import { Header3 } from '../ui/Header3';
import { rootStore } from '../models/Root.model';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 1em;
`;

export const Categories: React.FC = () => {
  const [categories, setCategories] = useLocalStorage<ClueCategory[] | null>('categories', null);

  useEffect(() => {
    if (categories != null && categories.length > 0) {
      rootStore.results.setCategoryId(categories[0].id);
      return;
    }

    void requestToApi<ClueCategory[]>('/categories?count=10').then((data) => {
      setCategories(data);

      rootStore.results.setCategoryId(data[0].id);
    });
  }, []);

  const changeCategory = useCallback((event: React.ChangeEvent<HTMLSelectElement>) => {
    rootStore.results.setCategoryId(+event.target.value);
  }, []);

  return (
    <Container>
      <Header3>Выберите категорию</Header3>
      {categories != null && (
        <select onChange={changeCategory}>
          {categories.map((category) => (
            <option key={category.id} value={category.id}>
              {category.title}
            </option>
          ))}
        </select>
      )}
    </Container>
  );
};
