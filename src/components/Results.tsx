import { Area } from '../ui/Area';
import { Info } from '../ui/Info';
import { rootStore } from '../models/Root.model';
import React, { useCallback } from 'react';
import { observer } from 'mobx-react-lite';
import { Button } from '../ui/Button';
import styled from 'styled-components';

const StyledButton = styled(Button)`
  margin-top: 1em;
`;

export const Results: React.FC = observer(function ResultsObserver() {
  const repeat = useCallback(() => {
    rootStore.setCategoryInfo(null);
    rootStore.results.reset();
  }, []);

  return (
    <Area>
      <Info>Игра окончена! Ваш результат: {rootStore.results.value}$</Info>
      <StyledButton onClick={repeat}>ЗАНОВО</StyledButton>
    </Area>
  );
});
