import React from 'react';
import styled from 'styled-components';
import { HeaderButton } from '../ui/HeaderButton';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
  margin-top: 0.5em;

  & > *:not(:first-child) {
    margin-left: 1em;
  }
`;

export const Header: React.FC = () => {
  return (
    <Container>
      <HeaderButton to="/">ГЛАВНАЯ</HeaderButton>
      <HeaderButton to="/play">ИГРАТЬ</HeaderButton>
    </Container>
  );
};
