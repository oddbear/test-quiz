import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Home } from '../pages/Home';
import { Header } from './Header';
import styled from 'styled-components';
import { MAIN_COLORS } from '../utils/constants';
import { Play } from '../pages/Play';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  max-width: 1200px;
  margin: 0 auto;
  font-size: 16px;
  font-family: sans-serif;
  color: ${MAIN_COLORS.primary};

  @media (max-width: 600px) {
    font-size: 25px;
  }
`;

export const Layout: React.FC = () => {
  return (
    <Container>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/play" element={<Play />} />
      </Routes>
    </Container>
  );
};
