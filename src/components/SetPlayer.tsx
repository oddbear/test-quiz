import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { Input } from '../ui/Input';
import useLocalStorage from 'use-local-storage';
import { Button } from '../ui/Button';
import { Info } from '../ui/Info';
import { NICK_LOCALSTORAGE_KEY } from '../utils/constants';

const Container = styled.div`
  font-family: inherit;
  font-size: inherit;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const StyledButton = styled(Button)`
  margin-top: 1em;
`;

const deprecatedSymbolsInNickRegexp = /[^a-zа-я0-9]/gi;

export const SetPlayer: React.FC = () => {
  const [, setNick] = useLocalStorage<string | null>(NICK_LOCALSTORAGE_KEY, null);
  const [tmpNick, setTmpNick] = useState<string>('');

  const onConfirm = useCallback(() => {
    setNick(tmpNick);
  }, [tmpNick]);

  const onChange = useCallback((value: string) => {
    if (deprecatedSymbolsInNickRegexp.test(value)) {
      value = value.replace(deprecatedSymbolsInNickRegexp, '');
    }

    setTmpNick(value);
  }, []);

  return (
    <Container>
      <Info>Перед началом игры выберите себе никнейм.</Info>
      <Input value={tmpNick} changeValue={onChange} placeholder="Введите никнейм" name="НИКНЕЙМ (мин. 3 символа)" />
      <StyledButton disabled={tmpNick.length < 3} onClick={onConfirm}>
        ПОДТВЕРДИТЬ
      </StyledButton>
    </Container>
  );
};
