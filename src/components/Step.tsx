import React, { useCallback, useMemo, useState } from 'react';
import { observer } from 'mobx-react-lite';
import { rootStore } from '../models/Root.model';
import { Area } from '../ui/Area';
import { Info } from '../ui/Info';
import { Input } from '../ui/Input';
import { Button } from '../ui/Button';
import styled from 'styled-components';
import { getLeaderboard, pushToLeaderboard } from '../utils/localstorage';
import useLocalStorage from 'use-local-storage';
import { NICK_LOCALSTORAGE_KEY, QUESTIONS_IN_TOTAL } from '../utils/constants';

const ButtonsContainer = styled.div`
  margin-top: 1em;

  ${Button}:not(:first-child) {
    margin-left: 0.5em;
  }
`;

const StyledInfo = styled(Info)`
  width: 100%;
`;

export const Step: React.FC = observer(function StepObserver() {
  const [nick] = useLocalStorage<string | null>(NICK_LOCALSTORAGE_KEY, null);
  const [answer, setAnswer] = useState('');
  const [correctAnswer, setCorrectAnswer] = useState<string | null>(null);

  const step = useMemo(() => rootStore.categoryInfo!.steps[rootStore.stepIndex!], [rootStore.stepIndex]);

  const abort = useCallback(() => {
    rootStore.abort(() => {
      const leaderboard = getLeaderboard();
      const maxScore = leaderboard.find(({ name }) => {
        if (name === nick) {
          return true;
        }
      });

      if (maxScore == null || maxScore.score < rootStore.results.value) {
        rootStore.resetStepIndex();
        rootStore.results.setIsNewRecord(true);
        pushToLeaderboard(nick!, rootStore.results.value);
      }
    });
  }, [nick]);

  const showAnswer = useCallback(() => {
    if (step.answer.toLowerCase() === answer.toLowerCase()) {
      rootStore.results.addValue(step.value);
    }

    setCorrectAnswer(step.answer);
  }, [answer, step]);

  const skipQuestion = useCallback(() => {
    setCorrectAnswer(step.answer);
  }, [step]);

  const confirm = useCallback(() => {
    setCorrectAnswer(null);
    setAnswer('');

    rootStore.nextStep();
  }, [step]);

  return (
    <Area>
      <StyledInfo>
        <b>Вопрос:</b>
        <br />
        <i>{step.question}</i>
      </StyledInfo>
      {correctAnswer == null ? (
        <Input value={answer} changeValue={setAnswer} placeholder="Введите ответ" name="ОТВЕТ" />
      ) : (
        <StyledInfo>
          <b>{correctAnswer === answer ? 'Верно!' : 'Неверно.'} Правильный ответ:</b>
          <br />
          <i>{correctAnswer}</i>
        </StyledInfo>
      )}
      <ButtonsContainer>
        {correctAnswer == null ? (
          <>
            <Button onClick={showAnswer} disabled={!answer}>
              ПОДТВЕРДИТЬ
            </Button>
            <Button onClick={skipQuestion}>ПРОПУСТИТЬ</Button>
          </>
        ) : (
          step.index + 1 < QUESTIONS_IN_TOTAL && <Button onClick={confirm}>ДАЛЬШЕ</Button>
        )}
        <Button theme="danger" onClick={abort}>
          ЗАВЕРШИТЬ
        </Button>
      </ButtonsContainer>
    </Area>
  );
});
