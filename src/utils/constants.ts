export const TRANSITION_TIME = '200ms';
export const NICK_LOCALSTORAGE_KEY = 'nick';

export const QUESTIONS_IN_TOTAL = 10;

export const MAIN_COLORS = {
  primary: '#ddd',
  secondary: '#222',
  danger: '#bd6262',
};
