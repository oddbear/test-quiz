export const requestToApi = async <R>(endpoint: string): Promise<R> => {
  const apiUrl = process.env.REACT_APP_CLUES_API_URL as string;

  try {
    const response = await fetch(`${apiUrl}/${endpoint.replace(/^\//, '')}`);
    const data = await response.json();
    return data as R;
  } catch (err) {
    alert('Возникла ошибка запроса, пожалуйста, обновите страницу и попробуйте снова.');
    throw err;
  }
};
