import z from 'zod';
import { Leaderboard } from '../types/Leaderboard.type';

const leaderboardSchema = z.array(
  z.object({
    name: z.string(),
    score: z.number(),
  }),
);

export const getLeaderboard = (): Leaderboard[] => {
  const leaderboard = localStorage.getItem('leaderboard');

  if (leaderboard == null) {
    return [];
  }

  try {
    const result = JSON.parse(leaderboard) as z.infer<typeof leaderboardSchema>;
    leaderboardSchema.parse(result);

    return result;
  } catch (err) {
    return [];
  }
};

export const pushToLeaderboard = (name: string, score: number): void => {
  const leaderboard = getLeaderboard().filter((entry) => entry.name !== name);

  leaderboard.push({
    name,
    score,
  });

  localStorage.setItem('leaderboard', JSON.stringify(leaderboard));
};
