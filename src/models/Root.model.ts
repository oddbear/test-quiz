import { cast, types } from 'mobx-state-tree';
import { ResultsModel } from './Results.model';
import { CategoryInfoModel } from './CategoryInfo.model';
import { ClueCategory } from '../types/Clue.type';
import { QUESTIONS_IN_TOTAL } from '../utils/constants';

const defaultValue = (value: number | null): number => value ?? 10;

const RootModel = types
  .model('Root', {
    categoryInfo: types.optional(types.maybeNull(CategoryInfoModel), null),
    stepIndex: types.optional(types.maybeNull(types.number), null),
    results: types.optional(ResultsModel, {}),
  })
  .actions((self) => ({
    setCategoryInfo(categoryInfo: ClueCategory | null) {
      if (categoryInfo == null) {
        self.categoryInfo = null;

        return;
      }

      const { clues } = categoryInfo;

      clues.sort((a, b) => (defaultValue(a.value) > defaultValue(b.value) ? 1 : -1));

      self.categoryInfo = {
        steps: cast(
          clues.map((clue, index) => ({
            index,
            question: clue.question,
            answer: clue.answer,
            value: defaultValue(clue.value),
          })),
        ),
      };
    },
    abort(finishCallback?: () => void) {
      finishCallback?.();

      self.stepIndex = 0;
      self.results.finished = true;
    },
    nextStep() {
      self.stepIndex = (self.stepIndex ?? -1) + 1;
    },
    resetStepIndex() {
      self.stepIndex = null;
    },
  }));

export const rootStore = RootModel.create();
