import { types } from 'mobx-state-tree';

export const ResultsModel = types
  .model('Results', {
    categoryId: types.optional(types.maybeNull(types.number), null),
    finished: types.optional(types.boolean, false),
    value: types.optional(types.number, 0),
    isNewRecord: types.optional(types.boolean, false),
  })
  .actions((self) => ({
    setCategoryId(categoryId: number) {
      self.categoryId = categoryId;
    },
    reset() {
      self.categoryId = null;
      self.finished = false;
      self.value = 0;
      self.isNewRecord = false;
    },
    addValue(value: number) {
      self.value += value;
    },
    setIsNewRecord(isNewRecord: boolean) {
      self.isNewRecord = isNewRecord;
    },
  }));
