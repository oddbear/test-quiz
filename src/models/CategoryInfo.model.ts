import { types } from 'mobx-state-tree';

const CategoryStep = types.model('CategoryStep', {
  index: types.number,
  question: types.string,
  answer: types.string,
  value: types.number,
});

export const CategoryInfoModel = types.model('CategoryInfo', {
  steps: types.array(CategoryStep),
});
