import React, { useMemo } from 'react';
import useLocalStorage from 'use-local-storage';
import { Leaderboard } from '../types/Leaderboard.type';
import { Area } from '../ui/Area';
import styled from 'styled-components';
import { Info } from '../ui/Info';

const Name = styled.span`
  font-weight: lighter;
  font-style: italic;
`;

const Score = styled.span`
  font-weight: bold;
`;

export const Home: React.FC = () => {
  const [leaderboard] = useLocalStorage<Leaderboard[]>('leaderboard', []);

  const sortedLeaderboard = useMemo(() => {
    return leaderboard.sort((a, b) => b.score - a.score);
  }, []);

  return (
    <Area>
      <Info>Доска победителей:</Info>
      {sortedLeaderboard.map((item, index) => (
        <div key={index}>
          <span>
            <Name>{item.name}</Name> - <Score>{item.score}$</Score>
          </span>
        </div>
      ))}
    </Area>
  );
};
