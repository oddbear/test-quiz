import React, { useCallback } from 'react';
import useLocalStorage from 'use-local-storage';
import { SetPlayer } from '../components/SetPlayer';
import { Area } from '../ui/Area';
import { Categories } from '../components/Categories';
import { Info } from '../ui/Info';
import { ActiveText } from '../ui/ActiveText';
import { Button } from '../ui/Button';
import { observer } from 'mobx-react-lite';
import { rootStore } from '../models/Root.model';
import { Results } from '../components/Results';
import { Step } from '../components/Step';
import { requestToApi } from '../utils/request';
import { ClueCategory } from '../types/Clue.type';
import { NICK_LOCALSTORAGE_KEY } from '../utils/constants';

export const Play: React.FC = observer(function PlayObserver() {
  const [nick, setNick] = useLocalStorage<string | null>(NICK_LOCALSTORAGE_KEY, null);

  const resetNick = useCallback(() => {
    setNick(null);
  }, []);

  const start = useCallback(async () => {
    const categoryInfo = await requestToApi<ClueCategory>(`category?id=${rootStore.results.categoryId!}`);

    rootStore.setCategoryInfo(categoryInfo);
    rootStore.nextStep();
  }, []);

  if (nick == null) {
    return (
      <Area>
        <SetPlayer />
      </Area>
    );
  }

  if (rootStore.results.finished) {
    return <Results />;
  }

  if (rootStore.stepIndex != null && rootStore.categoryInfo != null) {
    return <Step />;
  }

  return (
    <Area>
      <Info>Привет, {nick}! Задайте настройки перед игрой.</Info>
      <ActiveText onClick={resetNick}>Сбросить ник</ActiveText>
      <Categories />
      <Button disabled={rootStore.results.categoryId == null} onClick={start}>
        НАЧАТЬ
      </Button>
    </Area>
  );
});
