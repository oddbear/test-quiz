import styled from 'styled-components';
import { MAIN_COLORS } from '../utils/constants';
import React from 'react';

interface Props {
  children: React.ReactNode;
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 1em;
  border: 1px dashed ${MAIN_COLORS.primary};
  margin-top: 1em;
`;

export const Area: React.FC<Props> = ({ children }) => {
  return <Container>{children}</Container>;
};
