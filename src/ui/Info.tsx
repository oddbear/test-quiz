import styled from 'styled-components';

export const Info = styled.p`
  margin: 0 0 2em 0;
  font-size: 1.2em;
`;
