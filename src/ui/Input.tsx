import React, { useCallback } from 'react';
import styled from 'styled-components';
import { MAIN_COLORS } from '../utils/constants';

interface Props {
  value: string;
  changeValue: (value: string) => void;
  placeholder: string;
  name: string;
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Element = styled.input`
  border: none;
  border-bottom: 2px solid ${MAIN_COLORS.primary};
  border-radius: 2px;
  padding: 1em 0;
  outline: none;
  background-color: transparent;
  color: inherit;
  font-size: inherit;
  font-family: inherit;
  width: 100%;
`;

const NameField = styled.span`
  font-size: 0.7em;
`;

const Label = styled.label`
  display: flex;
  flex-direction: column;
`;

export const Input: React.FC<Props> = ({ value, changeValue, placeholder, name }) => {
  const onChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    changeValue(event.target.value);
  }, []);

  return (
    <Container>
      <Label>
        <NameField>{name}</NameField>
        <Element type="text" value={value} onChange={onChange} placeholder={placeholder} name={name} />
      </Label>
    </Container>
  );
};
