import styled from 'styled-components';

export const ActiveText = styled.span`
  border-bottom: 1px dashed;
  cursor: pointer;
`;
