import React from 'react';
import styled from 'styled-components';
import { MAIN_COLORS, TRANSITION_TIME } from '../utils/constants';

type Themes = 'primary' | 'danger';

interface Props {
  theme?: Themes;
  disabled?: boolean;
  onClick?: () => void;
  children: React.ReactNode;
  className?: string;
}

interface ContainerProps {
  theme?: Themes;
}

const Container = styled.button<ContainerProps>`
  padding: 1em 2em;
  outline: none;
  border: none;
  border-radius: 2px;
  background-color: ${({ theme }) => (theme === 'danger' ? MAIN_COLORS.danger : MAIN_COLORS.primary)};
  color: ${({ theme }) => (theme === 'danger' ? MAIN_COLORS.primary : MAIN_COLORS.secondary)};
  font-family: inherit;
  font-weight: bold;
  box-shadow: 0 0 0 0 ${MAIN_COLORS.primary};
  cursor: pointer;
  transition: color ${TRANSITION_TIME} ease-in-out, box-shadow ${TRANSITION_TIME} ease-in-out,
    transform ${TRANSITION_TIME} ease-in-out;

  &:not(:disabled) {
    &:hover {
      box-shadow: 0 0 0 2px ${MAIN_COLORS.primary};
    }

    &:active {
      transform: scale(0.95);
    }
  }

  &:disabled {
    cursor: default;
    color: #ccc;
  }

  a {
    color: inherit;
    text-decoration: none;
  }
`;

const ButtonElement: React.FC<Props> = ({ theme, onClick, disabled, children, className }) => {
  return (
    <Container theme={theme} onClick={onClick} disabled={disabled} className={className}>
      {children}
    </Container>
  );
};

export const Button = styled(ButtonElement)``;
