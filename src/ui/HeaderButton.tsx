import React from 'react';
import styled from 'styled-components';
import { MAIN_COLORS, TRANSITION_TIME } from '../utils/constants';
import { Link } from 'react-router-dom';

const Container = styled.div`
  a {
    color: inherit;
    text-decoration: none;
  }
`;

interface Props {
  to: string;
  children: React.ReactNode;
}

const Element = styled.button`
  padding: 1em 2em;
  outline: none;
  border: 1px solid ${MAIN_COLORS.primary};
  border-radius: 2px;
  background: none;
  color: inherit;
  font-family: inherit;
  box-shadow: 0 0 0 0 ${MAIN_COLORS.primary};
  cursor: pointer;
  transition: color ${TRANSITION_TIME} ease-in-out, box-shadow ${TRANSITION_TIME} ease-in-out,
    transform ${TRANSITION_TIME} ease-in-out;

  &:hover {
    color: #fff;
    box-shadow: 0 0 0 2px ${MAIN_COLORS.primary};
  }

  &:active {
    transform: scale(0.95);
  }
`;

export const HeaderButton: React.FC<Props> = ({ to, children }) => {
  return (
    <Container>
      <Link to={to}>
        <Element>{children}</Element>
      </Link>
    </Container>
  );
};
