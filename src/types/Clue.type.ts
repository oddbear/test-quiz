export interface ClueStep {
  id: number;
  answer: string;
  question: string;
  value: number;
  airdate: string;
  created_at: string;
  updated_at: string;
  category_id: number;
  game_id: number;
  invalid_count: null;
}

export interface ClueCategory {
  id: number;
  title: string;
  clues_count: number;
  clues: Clue[];
}

export interface Clue {
  id: number;
  answer: string;
  question: string;
  value: number | null;
  airdate: string;
  created_at: string;
  updated_at: string;
  category_id: number;
  game_id: number;
  invalid_count: number;
}
